<html>
<head>
    <title>Find by image</title>

    <style>
        body, html {
            padding: 0;
            margin: 0;
            width: 100vw;
            height: 100vh;
            background: #3d3d3d;
        }

        body {
            display: flex;
            flex-direction: column;
            justify-content: start;
            align-items: center;
            height: 100vh;
        }

        .box{
            background: #DDD;
            border-radius: 2%;
            width: 40vw;
            height: 20%;
            text-align: center;
            color: green;
            margin: 10%;
        }
    </style>

</head>

<body>

<form action="index.php" method="post" enctype="multipart/form-data">
    <div class="box">1)Select image to upload:<input type="file" name="fileToUpload" id="fileToUpload"></div>
    <div class="box">2)Click to upload image on server: <input type="submit" value="CLICK ME" name="submit"></div>

<div class="box">
<?php

if(isset($_POST['submit'])) {
    $target_file = getcwd() . '/' .basename($_FILES["fileToUpload"]["name"]);
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        
        $image = $_FILES["fileToUpload"]["name"];
        $comand = 'curl -s -F "image_url=" -F "image_content=" -F "filename=" -F "h1=en"  -F "bih=179" -F "biw=1600" -F "encoded_image=@'.$image.'" https://www.google.co.in/searchbyimage/upload';
        exec($comand, $res);
        echo $res[4];

        unlink($target_file);

    } else {
        echo "Sorry, there was an error uploading your file";
    }
} else echo "There will be result link";

?>

</div>

</form>

</body>

</html>